import React, {useState} from 'react'
import classes from './App.module.css';
import LoginForm from "./components/LoginForm"
import {getToken} from "./services/authentication"
import WelcomeScreen from "./components/WelcomeScreen"

function App() {
  const [token, setToken] = useState<string | null>(getToken())

  return (
    <div className={classes.App}>
      {!token && <LoginForm onSuccess={setToken} />}
      {token && <WelcomeScreen />}
    </div>
  );
}

export default App;
