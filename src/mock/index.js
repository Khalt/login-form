import { createServer, Response } from "miragejs"
import { AUTHENTICATION_ENDPOINT } from '../services/authentication'

createServer({
  routes() {
    this.post(AUTHENTICATION_ENDPOINT, (schema, request) => {
      const { username, password } = JSON.parse(request.requestBody)

      if (username === 'admin' && password === 'admin') return { token: 'sampleToken' }

      return new Response(
        400,
        {},
        { message: "Wrong credentials" }
      )
    })
  },
})
