import React from 'react';
import clsx from 'clsx';

import classes from './styles.module.css';

export interface ButtonProps extends React.ComponentPropsWithoutRef<'button'> {}

const Button = ({ className, ...rest }: ButtonProps) => {
    const buttonClassName = clsx(className, classes.button, { [classes.disabled]: rest.disabled });

    return <button className={buttonClassName} {...rest} />;
}

export default Button;
