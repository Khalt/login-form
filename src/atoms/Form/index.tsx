import React from 'react'
import clsx from 'clsx'

import classes from './styles.module.css'

export interface ButtonProps extends React.ComponentPropsWithoutRef<'form'> {}

const Form = ({className, children, ...rest}: ButtonProps) => (
  <form className={clsx(className, classes.form)} {...rest} >
    {children}
  </form>
)

export default Form
