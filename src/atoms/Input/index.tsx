import clsx from 'clsx';
import React from 'react';

import classes from './styles.module.css';

interface InputProps extends React.ComponentPropsWithRef<'input'> {}

const Input = React.forwardRef<HTMLInputElement, InputProps>(({ className, ...rest }, ref) => (
  <input className={clsx(className, classes.input)} ref={ref} {...rest} />
));

export default Input;
