import React, {ReactNode} from 'react'
import clsx from 'clsx'

import classes from './styles.module.css'

export interface ButtonProps extends React.ComponentPropsWithoutRef<'label'> {
  label: ReactNode;
}

const Label = ({className, label, children, ...rest}: ButtonProps) => (
  <label className={clsx(className, classes.labelContainer)} {...rest} >
    <span className={classes.labelText}>
      {label}
    </span>
    {children}
  </label>
)

export default Label
