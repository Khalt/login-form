import React, {useCallback, useRef, useState} from 'react'

import Form from "../../atoms/Form"
import {login} from "../../services/authentication"
import Label from "../../atoms/Label"
import Input from "../../atoms/Input"
import Button from "../../atoms/Button"

export interface LoginFormProps {
  onSuccess: (token: string) => void;
}

const LoginForm = ({onSuccess}: LoginFormProps) => {
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(null)
  const usernameFormRef = useRef<HTMLInputElement>(null)
  const passwordFormRef = useRef<HTMLInputElement>(null)

  const handleLogin = useCallback(async (e: React.FormEvent) => {
    e.preventDefault()
    if (loading || !usernameFormRef.current?.value || !passwordFormRef.current?.value) return
    setLoading(true)
    try {
      const token = await login({
        username: usernameFormRef.current.value,
        password: passwordFormRef.current.value,
      })
      onSuccess(token)
    } catch (error) {
      setError(error.message)
      setLoading(false)
    }
  }, [loading, onSuccess])

  return (
    <Form onSubmit={handleLogin}>
      <Label label="Username">
        <Input ref={usernameFormRef} />
      </Label>
      <Label label="Password">
        <Input ref={passwordFormRef} />
      </Label>
      <Button type="submit" disabled={loading}>
        LOG IN
      </Button>
      {error}
    </Form>
  )
}

export default LoginForm
