import React from 'react'

import {logout} from "../../services/authentication"
import Button from "../../atoms/Button"
import styles from "./styles.module.css"

const WelcomeScreen = () => (
  <div className={styles.welcomeScreen}>
    WELCOME TO MY APP, YOU ARE LOGGED IN, NOW YOU CAN
    <Button onClick={logout}>
      LOG OUT
    </Button>
  </div>
)


export default WelcomeScreen;
