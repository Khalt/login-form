import {Credentials} from "../models"
import {handleResponse} from "./api"

const TOKEN_STORAGE_KEY = 'token'

export const AUTHENTICATION_ENDPOINT = '/authenticate'

const storeToken = (token: string) => localStorage.setItem(TOKEN_STORAGE_KEY, token)
const clearToken = () => {
  localStorage.removeItem(TOKEN_STORAGE_KEY)
  window.location.reload()
}
export const getToken = () => localStorage.getItem(TOKEN_STORAGE_KEY)

export const login = async (credentials: Credentials) => {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(credentials)
  };

  const response = await fetch(AUTHENTICATION_ENDPOINT, requestOptions)
  const data = await handleResponse(response)

  const { token } = data
  storeToken(token)

  return token
}

export const logout = clearToken
