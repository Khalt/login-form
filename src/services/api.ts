import {logout} from "./authentication"

const HTTP_UNAUTHORIZED = 401
const HTTP_FORBIDDEN = 403

export const handleResponse = async (response: Response) => {
  const text = await response.text()
  const data = text && JSON.parse(text)

  if (response.ok) return data;

  [HTTP_UNAUTHORIZED, HTTP_FORBIDDEN].includes(response.status) && logout()
  const error = data?.message || response.statusText;
  if (error) throw new Error(error)

  return data
}
